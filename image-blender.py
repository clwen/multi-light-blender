from PIL import Image

def combine_images(img1, img2):
    w1, h1 = img1.size
    w2, h2 = img2.size
    assert w1 == w2 and h1 == h2

    pixels1 = img1.load()
    pixels2 = img2.load()
    combined = Image.new('RGB', (w1, h1))
    pixels_combined = combined.load()

    for y in xrange(h):
        for x in xrange(w):
            r = int(pixels1[x, y][0] + pixels2[x, y][0])
            g = int(pixels1[x, y][1] + pixels2[x, y][1])
            b = int(pixels1[x, y][2] + pixels2[x, y][2])
            pixels_combined[x, y] = (r, g, b)
    combined.show()
    combined.save('after.jpg')

def change_hue(image):
    pass

left = Image.open('left02.jpg')
w, h = left.size
pixels = left.load()

for y in xrange(h):
    for x in xrange(w):
        r = int(0 * pixels[x, y][0])
        g = int(0.5 * pixels[x, y][1])
        b = int(1 * pixels[x, y][2])
        pixels[x, y] = (r, g, b)

left.show()
left.save('left_after.jpg')

right = Image.open('right02.jpg')
w, h = right.size
pixels = right.load()

for y in xrange(h):
    for x in xrange(w):
        r = int(1 * pixels[x, y][0])
        g = int(0.5 * pixels[x, y][1])
        b = int(0 * pixels[x, y][2])
        pixels[x, y] = (r, g, b)

right.show()
right.save('right_after.jpg')

combine_images(left, right)
